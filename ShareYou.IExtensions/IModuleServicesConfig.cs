﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace ShareYou.IExtensions
{
    /// <summary>
    /// 模块服务配置
    /// </summary>
    public interface IModuleServicesConfig
    {
        /// <summary>
        /// 配置模块的相关服务
        /// </summary>
        /// <param name="services"></param>
        /// <param name="env"></param>
        void ConfigureServices(IServiceCollection services, IHostingEnvironment env);
    }
}

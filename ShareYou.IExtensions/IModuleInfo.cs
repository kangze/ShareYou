﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ShareYou.IExtensions
{
    public interface IModuleInfo
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        string Name { get; }

        /// <summary>
        /// 作者
        /// </summary>
        string Author { get; }

        /// <summary>
        /// 作者邮箱
        /// </summary>
        string AuthorEmail { get; }

        /// <summary>
        /// 作者个人主页
        /// </summary>
        string Url { get; }
    }
}

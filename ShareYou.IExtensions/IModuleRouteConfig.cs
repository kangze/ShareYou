﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;

namespace ShareYou.IExtensions
{
    public interface IModuleRouteConfig
    {
        /// <summary>
        /// 区域的名称
        /// </summary>
        string Area { get; }

        /// <summary>
        /// 配置路由
        /// </summary>
        /// <returns></returns>
        RouteBuilder CreateRouteBuilder(IApplicationBuilder app);
    }
}

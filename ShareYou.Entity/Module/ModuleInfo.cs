﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareYou.Entity.Module
{
    /// <summary>
    /// 模块信息
    /// </summary>
    public class ModuleInfo
    {

        public string Name { get; set; }

        public string Author { get; set; }

        public string AuthorEmail { get; set; }

        public string Url { get; set; }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ShareYou.Entity.Module.Dto
{
    public class ModuleAssemblyInfo
    {
        /// <summary>
        /// 模块的文件夹的名称
        /// </summary>
        public string FolderName { get; set; }

        /// <summary>
        /// 模块的程序集
        /// </summary>
        public Assembly Assembly { get; set; }

        /// <summary>
        /// 模块所在文件夹的位置
        /// </summary>
        public string FolderPath { get; set; }
    }
}

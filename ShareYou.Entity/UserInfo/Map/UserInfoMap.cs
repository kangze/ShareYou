﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ShareYou.Entity.UserInfo.Map
{
    /// <summary>
    /// 用户实体类的具体配置
    /// </summary>
    public class UserInfoMap:EntityTypeConfiguration<UserInfo>
    {
        public UserInfoMap()
        {
            this.HasKey(u => u.Id);
            this.Property(u => u.NormalizedUserName)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("UserNameIndex") { IsUnique = true }));

            this.Property(u => u.UserName)
                .IsRequired()
                .HasMaxLength(64)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("UserName1Index") {IsUnique = true}));

            this.Property(u => u.NormalizedEmail)
                .IsRequired()
                .HasMaxLength(128)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("UserEmailIndex") { IsUnique = true }));

            this.Property(u => u.Email)
                .IsRequired()
                .HasMaxLength(128)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("UserEmail1Index") { IsUnique = true }));

            this.ToTable("SY_UserInfo");
            this.Property(u => u.ConcurrencyStamp).IsConcurrencyToken(true);
        }
    }
}

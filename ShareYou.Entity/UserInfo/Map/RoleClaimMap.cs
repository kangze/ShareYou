﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareYou.Entity.UserInfo.Map
{
    public class RoleClaimMap:EntityTypeConfiguration<RoleClaim>
    {
        public RoleClaimMap()
        {
            this.HasKey(u => u.Id);
            this.ToTable("SY_RoleClaim");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ShareYou.Entity.UserInfo.Map
{
    public class UserTokenMap:EntityTypeConfiguration<UserToken>
    {
        public UserTokenMap()
        {
            this.HasKey(u => new { u.UserId, u.LoginProvider, u.Name });
            this.ToTable("SY_UserToken");
        }
    }
}

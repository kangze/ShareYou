﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareYou.Entity.UserInfo.Map
{
    public class UserLoginMap:EntityTypeConfiguration<UserLogin>
    {
        public UserLoginMap()
        {
            this.HasKey(u => new { u.ProviderKey, u.LoginProvider });
            this.ToTable("SY_UserLogin");
        }
    }
}

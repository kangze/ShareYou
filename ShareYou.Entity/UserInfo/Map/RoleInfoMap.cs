﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareYou.Entity.UserInfo.Map
{
    public class RoleInfoMap:EntityTypeConfiguration<RoleInfo>
    {
        public RoleInfoMap()
        {
            this.HasKey(u => u.Id);
            this.ToTable("SY_RoleInfo");
            this.Property(u => u.Name)
                .IsRequired()
                .HasMaxLength(32)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("RoleNameIndex") {IsUnique = true}));
            this.Property(u => u.ConcurrencyStamp).IsConcurrencyToken();
            this.Property(u => u.NormalizedName).HasMaxLength(32).IsRequired();
        }
    }
}

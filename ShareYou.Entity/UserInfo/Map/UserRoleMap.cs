﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareYou.Entity.UserInfo.Map
{
    public class UserRoleMap:EntityTypeConfiguration<UserRole>
    {
        public UserRoleMap()
        {
            this.HasKey(u => new { u.UserId, u.RoleId });
            this.ToTable("SY_UserRole");
        }
    }
}

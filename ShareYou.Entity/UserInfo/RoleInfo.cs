﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AspNetCore.Identity.EntityFramework6;

namespace ShareYou.Entity.UserInfo
{
    /// <summary>
    /// 用户中的角色实体,和用户多对多之间的管理，所以会存在一个中间表
    /// </summary>
    public class RoleInfo:IdentityRole<int,IdentityUserRole,IdentityRoleClaim>
    {
        public RoleInfo()
        {
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AspNetCore.Identity.EntityFramework6;

namespace ShareYou.Entity.UserInfo
{
    /// <summary>
    /// 重写identity中所使用的用户的类型
    /// </summary>
    public class UserInfo : IdentityUser<int,IdentityUserClaim,IdentityUserRole,IdentityUserLogin>
    {
        public UserInfo()
        {
            
        }
        /// <summary>
        /// 用户的注册时间
        /// </summary>
        public DateTime RegisterDateTime { get; set; }
    }
    
}

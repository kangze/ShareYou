﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.DependencyInjection;

namespace ShareYou.Configuration
{
    public static class ModuleInit
    {
        public static IServiceCollection ConfigRazorViewPath(this IServiceCollection services)
        {
            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationExpanders.Add(new RazorViewPathConfig());
            });
            return services;
        }
    }
}

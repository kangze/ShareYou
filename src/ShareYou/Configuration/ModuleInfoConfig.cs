﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using NuGet.Packaging;
using ShareYou.Entity.Module;
using ShareYou.IExtensions;
using ShareYou.IServices.Module;

namespace ShareYou.Configuration
{
    public static class ModuleInfoConfig
    {
        /// <summary>
        /// 配置模块的路由设置
        /// </summary>
        /// <param name="app"></param>
        /// <param name="routeConfigs"></param>
        /// <returns></returns>
        public static IApplicationBuilder ConfigModuleRoute(this IApplicationBuilder app, IList<IModuleRouteConfig> routeConfigs)
        {
            if (routeConfigs == null)
                throw new NullReferenceException();
            //验证路由配置有效性
            IList<IRouter> listRouter = new List<IRouter>();
            foreach (IModuleRouteConfig config in routeConfigs)
            {
                //if(config.CreateRouteBuilder())
                var routeBuilder = config.CreateRouteBuilder(app);
                if (null == routeBuilder)
                    throw new Exception("CreateRouteBuilder不能返回为null");
                //  module/home/index
                foreach (IRouter route in routeBuilder.Routes)
                {
                    var router = route as Route;
                    if (null == router)
                    {
                        throw new Exception("RouteBuilder类型中Routes属性必须是Route的实现");
                    }
                    if (!router.RouteTemplate.StartsWith(config.Area, StringComparison.InvariantCultureIgnoreCase))
                    {
                        throw new Exception("路由模板必须以Area开头");
                    }
                }
                listRouter.Add(routeBuilder.Build());
            }
            IRouteBuilder builder = new RouteBuilder(app);
            //builder.DefaultHandler 不需要设置
            builder.Routes.AddRange(listRouter);

            //配置路由中间件
            app.UseRouter(builder.Build());
            return null;
        }

        /// <summary>
        /// 模块信息配置(保存)
        /// </summary>
        public static IServiceCollection SaveModuleInfo(this IServiceCollection services, IList<IModuleInfo> list)
        {
            if (null == list)
                throw new NullReferenceException();
            var moduleService = services.BuildServiceProvider().GetService<IModuleInfoService>();
            foreach (IModuleInfo info in list)
            {
                moduleService.Add(new ModuleInfo()
                {
                    Name = info.Name,
                    Author = info.Author,
                    AuthorEmail = info.AuthorEmail,
                    Url = info.Url
                });
            }

            return services;
        }

        /// <summary>
        /// 模块的服务的配置
        /// </summary>
        /// <param name="services"></param>
        /// <param name="env"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static IServiceCollection RegisterModuleService(this IServiceCollection services,
            IHostingEnvironment env, IList<IModuleServicesConfig> list)
        {
            if(null==list)
                throw new NullReferenceException();
            foreach (IModuleServicesConfig config in list)
            {
                config.ConfigureServices(services,env);
            }
            return services;
        }
    }
}

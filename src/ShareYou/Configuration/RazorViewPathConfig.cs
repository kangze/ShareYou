﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Razor;

namespace ShareYou.Configuration
{
    public class RazorViewPathConfig:IViewLocationExpander
    {
        private readonly string _module = "module";

        public void PopulateValues(ViewLocationExpanderContext context)
        {
            //throw new NotImplementedException();
            if (context.ActionContext.ActionDescriptor.DisplayName.Contains(".Module."))
            {
                //组件程序
                var moduleName = context.ActionContext.ActionDescriptor.DisplayName.Split('.')[2];
                context.Values[_module] = moduleName;
            }
        }

        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            if (context.Values.ContainsKey(_module))
            {
                //组件程序
                var moduleName = context.Values[_module];
                var viewPaths = new string[]
                {
                    "/Modules/ShareYou.Module." + moduleName + "/Views/{1}/{0}.cshtml",
                    "/Modules/ShareYou.Module." + moduleName + "/Views/{0}.cshtml",
                };
                viewLocations = viewPaths.Concat(viewLocations);
            }
            return viewLocations;
        }
    }
}

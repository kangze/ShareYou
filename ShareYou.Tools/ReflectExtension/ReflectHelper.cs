﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ShareYou.Tools.ReflectExtension
{
    public static class ReflectHelper
    {
        /// <summary>
        /// 获取程序集中实现T（接口或者抽象类）的实现类型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assmebly"></param>
        /// <returns></returns>
        public static IList<T> GetClasses<T>(Assembly assmebly) where T:class
        {
            if(null==assmebly)
                throw new NullReferenceException();
            var type = typeof(T);
            if(!type.IsAbstract||!type.IsInterface)
                throw new Exception("类型必须是抽象类或者接口");
            IList<T> result=new List<T>();
            var types = assmebly.GetTypes().Where(u => u.GetInterface(type.FullName)!=null);
            foreach (Type impType in types)
            {
                result.Add((T)assmebly.CreateInstance(impType.FullName));
            }
            return result;
        }

        //public static IList<Assembly> LoadAssmeblys
    }
}

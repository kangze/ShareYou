﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShareYou.Message.Model;

namespace ShareYou.Message.Framework
{
    /// <summary>
    /// 消息存储(具有双重意义,既是业务逻辑，也是存储逻辑)
    /// </summary>
    /// <typeparam name="TMessage"></typeparam>
    public abstract class MessageSaver<TMessage>
        where TMessage:BaseMessage
    {

        /// <summary>
        /// 存储一条消息
        /// </summary>
        /// <param name="message"></param>
        public abstract void  SaveMessage(TMessage message);

        /// <summary>
        /// 获取还没有发送的消息
        /// </summary>
        /// <param name="type"></param>
        public abstract void GetUnResolvedMessage(string type);


    }
    
}

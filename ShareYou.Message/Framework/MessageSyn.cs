﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShareYou.Message.Model;

namespace ShareYou.Message.Framework
{
    /// <summary>
    /// 消息同步对象,一个同步对象，对应一种类型type的消息
    /// </summary>
    public abstract  class MessageSyn<TMessage,TEntity,TResult,TMessageSender,TMessageSaver>
        where TMessage:BaseMessage
        where TMessageSender:MessageSender<TMessage,TEntity,TResult>
        where TMessageSaver:MessageSaver<TMessage>
        where TEntity:class
        where TResult:class
    {
        public TMessageSender _sender;
        public TMessageSaver _saver;
        private readonly string _type;

        /// <summary>
        /// 构造:必须初始上面的三个对象
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="saver"></param>
        /// <param name="type"></param>
        public MessageSyn(TMessageSender sender, TMessageSaver saver,string type)
        {
            this._sender = sender;
            this._saver = saver;
            this._type = type;
        }

    }
}

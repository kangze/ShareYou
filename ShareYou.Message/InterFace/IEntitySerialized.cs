﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareYou.Message.InterFace
{
    public interface IEntitySerialized<TEntity, TResult>
        where TEntity : class
        where TResult : class
    {
        /// <summary>
        /// 对实体进行相关的序列化，标识的要送的对象
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        string Serialized(TEntity entity);

        /// <summary>
        /// 对响应的内容进行响应的反序列化为一个对象
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        TResult DeSerailized(string result);
    }
}

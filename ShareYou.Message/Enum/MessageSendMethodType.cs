﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareYou.Message.Enum
{
    public enum MessageSendMethodType
    {
        [Description("POST")]
        Post=0,

        [Description("GET")]
        Get=10,
    }
}

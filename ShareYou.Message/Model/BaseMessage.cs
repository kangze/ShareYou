﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareYou.Message.Model
{
    /// <summary>
    /// 消息框架所使用的消息基类
    /// </summary>
    public class BaseMessage
    {
        /// <summary>
        /// 消息的id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 消息的标识
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 消息所关联的对象的Id(一般是数据库实体)
        /// </summary>
        public string ObjectId { get; set; }

        /// <summary>
        /// 标识消息是否已经完结了
        /// </summary>
        public bool Finished { get; set; }

        /// <summary>
        /// 消息的附加内容(任意的格式)
        /// </summary>
        public string Content { get; set; }
    }
}

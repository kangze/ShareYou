CREATE TABLE [dbo].[SY_RoleClaim] (
    [Id] [int] NOT NULL IDENTITY,
    [RoleId] [int] NOT NULL,
    [ClaimType] [nvarchar](max),
    [ClaimValue] [nvarchar](max),
    CONSTRAINT [PK_dbo.SY_RoleClaim] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_RoleId] ON [dbo].[SY_RoleClaim]([RoleId])
CREATE TABLE [dbo].[SY_RoleInfo] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [nvarchar](256),
    [NormalizedName] [nvarchar](256),
    [ConcurrencyStamp] [nvarchar](max),
    CONSTRAINT [PK_dbo.SY_RoleInfo] PRIMARY KEY ([Id])
)
CREATE UNIQUE INDEX [RoleNameIndex] ON [dbo].[SY_RoleInfo]([NormalizedName])
CREATE TABLE [dbo].[SY_UserRole] (
    [UserId] [int] NOT NULL,
    [RoleId] [int] NOT NULL,
    CONSTRAINT [PK_dbo.SY_UserRole] PRIMARY KEY ([UserId], [RoleId])
)
CREATE INDEX [IX_UserId] ON [dbo].[SY_UserRole]([UserId])
CREATE INDEX [IX_RoleId] ON [dbo].[SY_UserRole]([RoleId])
CREATE TABLE [dbo].[SY_UserClaim] (
    [Id] [int] NOT NULL IDENTITY,
    [UserId] [int] NOT NULL,
    [ClaimType] [nvarchar](max),
    [ClaimValue] [nvarchar](max),
    CONSTRAINT [PK_dbo.SY_UserClaim] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_UserId] ON [dbo].[SY_UserClaim]([UserId])
CREATE TABLE [dbo].[SY_UserLogin] (
    [LoginProvider] [nvarchar](128) NOT NULL,
    [ProviderKey] [nvarchar](128) NOT NULL,
    [ProviderDisplayName] [nvarchar](max),
    [UserId] [int] NOT NULL,
    CONSTRAINT [PK_dbo.SY_UserLogin] PRIMARY KEY ([LoginProvider], [ProviderKey])
)
CREATE INDEX [IX_UserId] ON [dbo].[SY_UserLogin]([UserId])
CREATE TABLE [dbo].[SY_UserInfo] (
    [Id] [int] NOT NULL IDENTITY,
    [RegisterDateTime] [datetime] NOT NULL,
    [UserName] [nvarchar](256),
    [NormalizedUserName] [nvarchar](256),
    [Email] [nvarchar](256),
    [NormalizedEmail] [nvarchar](256),
    [EmailConfirmed] [bit] NOT NULL,
    [PasswordHash] [nvarchar](max),
    [SecurityStamp] [nvarchar](max),
    [ConcurrencyStamp] [nvarchar](max),
    [PhoneNumber] [nvarchar](max),
    [PhoneNumberConfirmed] [bit] NOT NULL,
    [TwoFactorEnabled] [bit] NOT NULL,
    [LockoutEnd] [datetimeoffset](7),
    [LockoutEnabled] [bit] NOT NULL,
    [AccessFailedCount] [int] NOT NULL,
    CONSTRAINT [PK_dbo.SY_UserInfo] PRIMARY KEY ([Id])
)
CREATE UNIQUE INDEX [UserNameIndex] ON [dbo].[SY_UserInfo]([NormalizedUserName])
CREATE UNIQUE INDEX [EmailIndex] ON [dbo].[SY_UserInfo]([NormalizedEmail])
CREATE TABLE [dbo].[SY_UserToken] (
    [UserId] [int] NOT NULL,
    [LoginProvider] [nvarchar](128) NOT NULL,
    [Name] [nvarchar](128) NOT NULL,
    [Value] [nvarchar](max),
    CONSTRAINT [PK_dbo.SY_UserToken] PRIMARY KEY ([UserId], [LoginProvider], [Name])
)
ALTER TABLE [dbo].[SY_RoleClaim] ADD CONSTRAINT [FK_dbo.SY_RoleClaim_dbo.SY_RoleInfo_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[SY_RoleInfo] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[SY_UserRole] ADD CONSTRAINT [FK_dbo.SY_UserRole_dbo.SY_RoleInfo_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[SY_RoleInfo] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[SY_UserRole] ADD CONSTRAINT [FK_dbo.SY_UserRole_dbo.SY_UserInfo_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[SY_UserInfo] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[SY_UserClaim] ADD CONSTRAINT [FK_dbo.SY_UserClaim_dbo.SY_UserInfo_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[SY_UserInfo] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[SY_UserLogin] ADD CONSTRAINT [FK_dbo.SY_UserLogin_dbo.SY_UserInfo_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[SY_UserInfo] ([Id]) ON DELETE CASCADE
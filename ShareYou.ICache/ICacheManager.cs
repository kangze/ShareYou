﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareYou.ICache
{
    public interface ICacheManager
    {
        /// <summary>
        /// 获取缓存值,如果没有则返回类型默认值default(T)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        T Get<T>(string key);
        /// <summary>
        /// 获取缓存值,如果没有获取到则返回所设置的默认值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        T Get<T>(string key, T defaultValue);

        /// <summary>
        /// 获取缓存值,如果没有,则返回Func返回的值,并加入新的缓存中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="func"></param>
        /// <returns></returns>
        T Get<T>(string key, Func<T> func);

        /// <summary>
        /// 获取缓存值,如果没有,则返回Func返回的值,并加入新的缓存中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="func"></param>
        /// <param name="absoluteDateTime"></param>
        /// <returns></returns>
        T Get<T>(string key, Func<T> func, DateTime absoluteDateTime);

        /// <summary>
        /// 获取缓存值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        IList<T> Gets<T>(string key);


        IList<T> Gets<T>(string key, IList<T> defaultValue);

        IList<T> Gets<T>(string key, Func<IList<T>> func);

        IList<T> Gets<T>(string key, Func<IList<T>> func, DateTime absoluteDateTime);

        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        void Set(string key, object value);

        /// <summary>
        /// 设置缓存,并设置绝对过期时间
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="absoluteDateTime"></param>
        void Set(string key, object value, DateTime absoluteDateTime);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShareYou.ICache;

namespace ShareYou.CacheMemory
{
    /// <summary>
    /// Cache实现类
    /// </summary>
    public class CacheForMemory:ICacheManager   
    {
        public T Get<T>(string key)
        {
            throw new NotImplementedException();
        }

        public T Get<T>(string key, T defaultValue)
        {
            throw new NotImplementedException();
        }

        public T Get<T>(string key, Func<T> func)
        {
            throw new NotImplementedException();
        }

        public T Get<T>(string key, Func<T> func, DateTime absoluteDateTime)
        {
            throw new NotImplementedException();
        }

        public IList<T> Gets<T>(string key)
        {
            throw new NotImplementedException();
        }

        public IList<T> Gets<T>(string key, IList<T> defaultValue)
        {
            throw new NotImplementedException();
        }

        public IList<T> Gets<T>(string key, Func<IList<T>> func)
        {
            throw new NotImplementedException();
        }

        public IList<T> Gets<T>(string key, Func<IList<T>> func, DateTime absoluteDateTime)
        {
            throw new NotImplementedException();
        }

        public void Set(string key, object value)
        {
            throw new NotImplementedException();
        }

        public void Set(string key, object value, DateTime absoluteDateTime)
        {
            throw new NotImplementedException();
        }
    }
}

﻿import React, { Component, PropTypes } from 'react'
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
//导入控件集合
import CategoryContent from './components/containers/CategoryContent'
import HomeContent from './components/containers/HomeContent'
import RecommandContent from './components/containers/RecommandContent'
import Nav from './components/Nav'

import Store from './store/'
import Actions from './actions';

class Home extends Component {
    constructor() {
        super();
    }

    //容器组件渲染完毕之后请求服务端的数据
    componentDidMount() {
        //请求分类信息
        this.props.Actions.Category.loadCategory();
        //请求博文信息
        this.props.Actions.Post.loadPost();
        
    }


    selectCategory(id) {
        this.props.Actions.Category.selectCategory(id);
    }
    likePost(id){
        this.props.Actions.Post.likePost(id);
    }
    render() {
        return (
            <div className="container-fluid">
            <Nav></Nav>
            <div className="row">
                <CategoryContent categories={this.props.Categories} selectCategory={this.selectCategory.bind(this)}></CategoryContent>
                <HomeContent posts={this.props.Posts} likePost={this.likePost.bind(this)}></HomeContent>
                <RecommandContent></RecommandContent>    
            </div>
            </div>
            )
    
    }
}
function mapStateToProps(state,props) {//props容器组件的this.props对象
    return state;//返回全的操作
}
function mapDispatchToProps(dispatch) {
    return {
        Actions: {
            Category: bindActionCreators(Actions.Category, dispatch),
            Post:bindActionCreators(Actions.Post,dispatch)
        }
    }
}

const App = connect(mapStateToProps, mapDispatchToProps)(Home);

let rootElement = document.getElementById("app");
render(
    <Provider store={Store}>
        <App/>
    </Provider>,rootElement
    )
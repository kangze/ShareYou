﻿import {combineReducers} from 'redux';
import Categories from './category';
import Posts from './post'
import RecommandUsers from './recommanduser'
import RecommandPost from './recommandpost'

const rootReducer = combineReducers({
    Categories,
    Posts,
    RecommandPost,
    RecommandUsers
});

export default rootReducer;
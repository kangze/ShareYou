import ActionTypes from '../constants/ActionTypes';

const initState=[];

export default function Posts(state=initState,action){
    switch(action.type){
        case ActionTypes.Post.LOAD:
            //加载初始的数据
            state=action.Posts;
            return state;

        case ActionTypes.Post.DELETE:
            //删除展示的博文
            state=Object.assign([],state);
            state.filter(ele=>{
                return ele.id!==action.id
            })
            return state;
        case ActionTypes.Post.LIKE:
            //点赞博文
            state=Object.assign([],state);
            state.forEach(ele=>{
                ele.like+=ele.id===action.id?1:0;
            })
            return state;

        case ActionTypes.Post.LOADNEXTPAGE:
            //填充博文数据
            state=Object.assign([],state);
            state=state.concat(action.posts);
            return state;

        default:
            return state;
    }
}
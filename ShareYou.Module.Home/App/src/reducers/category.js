﻿import ActionTypes from '../constants/ActionTypes';

const initState = [];
export default function Categories(state=initState, action) {
    switch (action.type) {
        case ActionTypes.Category.LOAD: //初始化category一栏的信息
            state = action.categories;
            return state;

        case ActionTypes.Category.SELECT:
            state = Object.assign([], state);
            state.forEach(x => {
                x.select=x.id===action.id;
            });
            return state;

        default:
            return state;
    }
};
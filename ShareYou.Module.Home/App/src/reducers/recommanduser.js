﻿import ActionTypes from '../constants/ActionTypes';


const initState=[]

export default function RecommandUsers(state=initState,action)
{
    switch(action.type){
        case ActionTypes.RecomandUser.LOAD:
            //初始化推荐用户
            state=action.RecommandUsers;
            return state;

        case ActionTypes.RecomandUser.SELECT:
            //选中推荐用户
            state=Object.assign([],state);
            state.forEach(ele=>{
                ele.select=ele.id===action.id;
            })
            return state;
        default:
            return state;
    }
}
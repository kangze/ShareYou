﻿import ActionTypes from '../constants/ActionTypes';


const initState=[];

export default function RecommandPost(state=initState,action) 
{
    switch(action.type){
        case ActionTypes.RecomandPost.LOAD:
            //初始化推荐博文
            state=action.RecommandPosts;
            return state;

        case ActionTypes.RecomandPost.SELECT:
            //选中推荐博文
            state=Object.assign([],state);
            state.forEach(ele=>{
                ele.select=ele.id===action.id;
            })
            return state;
            
        default:
            return state;
    }    
}
﻿import React, {Component, PropTypes} from 'react';
import CategoryMenu from '../CategoryMenu';

export default class CategoryContent extends Component {
    render() {
        return (
            <div className="col-md-2" id="menu">
            <div>
                <button type="button" className="btn btn-info">Click ME</button>
            </div>
            <CategoryMenu categories={this.props.categories} selectCategory={this.props.selectCategory} />
        </div>
            );
    }
}
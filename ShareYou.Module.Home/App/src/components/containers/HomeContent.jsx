﻿import React, {Component, PropTypes} from 'react';
import BreadNav from '../BreadNav';
import FriendTitle from '../FriendTitle';
import PostItem from '../PostItem';

export default class HomeContent extends  Component {
    render() {
        return (
            <div id="content" className="col-md-8">
                <BreadNav/>
                <FriendTitle/>
                {
                    this.props.posts.map((ele,index)=>{
                        return (<PostItem likePost={this.props.likePost} post={ele} key={index}/>)
                    })
                }
            </div>
            )
    }
}
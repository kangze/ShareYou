﻿import React, {Component, PropTypes} from 'react';

export default class PostItem extends  Component {


    handleLikeClick(id){
        this.props.likePost(id);
    }

    render() {
        return (<div className="card">
                    <img className="img-rounded pull-left" src={this.props.post.img} />
                    <h6>{this.props.post.title}</h6>
                    <span>{this.props.post.content}</span>
                    <div className="clearboth"></div>
                    <div>
                        <ul className="list-group">
                            <li className="list-group-item" onClick={this.handleLikeClick.bind(this,this.props.post.id)}>{this.props.post.like}</li>
                            <li className="list-group-item">Dapibus ac facilisis in</li>
                            <li className="list-group-item">Morbi leo risus</li>
                        </ul>
                    </div>
                </div>)
                
    }
}
﻿import React, { Component, PropTypes } from 'react';


//分类菜单信息
export default class CategoryMenu extends Component {

    handleClick(id) {
        this.props.selectCategory(id);
    }

    render() {
        return (
            <div>
                <ul className="list-group" >
                    {this.props.categories.map((x,index) => {
                        let activeStr="list-group-item";
                        if(x.select){
                            activeStr+=" active";
                        }
                        return (<li onClick={this.handleClick.bind(this,x.id)} key={index} data-id={x.id} className={activeStr}>
                            <i className="icon-camera-retro"></i> 
                            {x.name}</li>)
                    })}
                    
                </ul>
            </div>
        )
    }
}
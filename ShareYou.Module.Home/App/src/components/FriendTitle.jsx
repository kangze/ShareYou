﻿import React, {Component, PropTypes} from 'react';

export default class FriendTitle extends  Component {
    render() {
        return (
            <blockquote className="blockquote">
                    <p>努力不一定成功，但是不努力一定会轻松喔！</p>
                </blockquote>
            );
    }
}
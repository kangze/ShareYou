﻿//定义程序所有action的类型type
const ActionTypes= {
    Category: {
        LOAD:"CATEGORY.LOAD",                    //加载分类信息
        SELECT:"CATEGORY.SELECT"                 //选中分类
    },
    Post: {
        LOAD:"POST.LOAD",                    //初次加载文章
        DELETE: "POST.DELETE",               //删除文章
        LIKE: "POST.LIKE",                   //点赞文章
        LOADNEXTPAGE:"POST.LOADNEXTPAGE"     //加载下一页文章
    },
    RecomandUser: {
        LOAD: "RECOMMANDUSER.LOAD",                    //加载推荐用户
        SELECT: "RECOMMANDUSER.SELECT"                 //选中
    },
    RecomandPost: {
        LOAD: "RECOMANDPOST.LOAD",                    //加载推荐文章
        SELECT:"RECOMANDPOST.SELECT"                 //选中
    }

}
module.exports = ActionTypes;
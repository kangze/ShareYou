﻿import ActionTypes from '../constants/ActionTypes';

//加载所有分类信息
export function loadCategory() {
    //异步thunk
    return (dispatch) => {
        var arr = [
            {name:".Net",id:1,url:"/category/index"},
            {name:"Java",id:2,url:"/category/index"},
            {name:"PHP",id:3,url:"/category/index"}
        ];//暂时代替服务端请求
        dispatch({ type: ActionTypes.Category.LOAD, categories: arr });
    }
}

//选中某个分类
export function selectCategory(id) {
    return (dispatch) => {
        dispatch({ type: ActionTypes.Category.SELECT, id: id });
    }
}
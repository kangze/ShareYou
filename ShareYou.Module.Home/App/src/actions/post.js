﻿import ActionTypes from '../constants/ActionTypes';

//初始化加载博文信息代表网络请求
export function loadPost(){
    return (dispatch)=>{
        var arr=[
            {id:1,title:"我不放?",content:"这是我生",img:"/home/1.png",like:1},
            {id:2,title:"只以前的了",content:"这是我生",img:"/home/1.png",like:1},
            {id:3,title:"让",content:"这是我生来的",img:"/home/1.png",like:1},
        ]
        dispatch({type:ActionTypes.Post.LOAD,Posts:arr});
    }
}

//点赞某一篇博文
export function likePost(id){
    return (dispatch)=>{
        dispatch({type:ActionTypes.Post.LIKE,id:id});
    }
}

//删除某一篇博文
export function deletePost(id){
    return (dispatch)=>{
        dispatch({type:ActionTypes.Post.DELETE,id:id});
    }
}

//加载下一篇博文
export function loadNextPost(pageIndex){
    return (dispatch)=>{
        var arr=[
            {id:4,title:"我不能",content:"这是我生",img:"/home/1.png",like:1},
            {id:5,title:"只要放了",content:"这是我",img:"/home/1.png",like:1},
            {id:6,title:"让那些笑我",content:"这是我生来的使",img:"/home/1.png",like:1},
        ]
        dispatch({type:ActionTypes.Post.LOADNEXTPAGE,Posts:arr});
    }
}
﻿import * as Category from './category';
import * as Post from './post'

import { bindActionCreators } from 'redux';
//封装所有的action Creaters
const Actions = {
    Category,
    Post
}

export default Actions;
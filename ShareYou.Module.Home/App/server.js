﻿var webpack = require("webpack");
var WebpackDevServer = require("webpack-dev-server");
var config = require("./webpack.config.js");

new WebpackDevServer(webpack(config),
{
    publicPath: config.output.publicPath,
    stats: {
        colors: true
    },
    hot: true,
    historyApiFallback: true
}).listen(8080, "127.0.0.1", function (err, result) {
    if (err) {
        console.log(result);
    }
    console.log("监听------>Listening at localhost:8080");
});
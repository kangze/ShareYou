﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.DataAnnotations.Internal;
using ShareYou.Entity.UserInfo;

namespace ShareYou.Module.Home.Controllers
{
    public  class AccountController:Controller
    {
        private readonly UserManager<UserInfo> _userManager;
        private readonly SignInManager<UserInfo> _signInManager;

        public AccountController(SignInManager<UserInfo> signInManager, UserManager<UserInfo> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public IActionResult Test()
        {
            var user = this._userManager.Users.ToList();
            return Content("demo");
        }
        
    }
}

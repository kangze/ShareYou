﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AspNetCore.Identity.EntityFramework6;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using ShareYou.Entity.UserInfo;
using ShareYou.EntityFramework.ShareYouDb;
using ShareYou.IExtensions;

namespace ShareYou.Module.Home
{
    /// <summary>
    /// 模块配置文件
    /// </summary>
    public class Startup: IModuleServicesConfig, IModuleRouteConfig, IModuleInfo
    {
        public void ConfigureServices(IServiceCollection services, IHostingEnvironment env)
        {



            //MvcServiceCollectionExtensions
            //添加asp.net core　中MVC框架中以来的服务
            services.AddMvc().AddApplicationPart(Assembly.GetExecutingAssembly());//.AddControllersAsServices();

            //设置identity相关的配置
            services.AddIdentity<UserInfo, RoleInfo>(option =>
            {
                option.Cookies.ApplicationCookie.CookieName = "ShareYou.Host"; //设置主cookie得名称
            })
                .AddEntityFrameworkStores<SyDbContext, int>()
                .AddDefaultTokenProviders();
        }

        public string Area
        {
            get { return "Home"; }
        }

        public RouteBuilder CreateRouteBuilder(IApplicationBuilder app)
        {
            app.UseIdentity();
            var builder=new RouteBuilder(app);
            //home模块默认使用mvc这个框架
            builder.DefaultHandler = app.ApplicationServices.GetRequiredService<MvcRouteHandler>();
            builder.MapRoute("Home_default", "Home/{controller=Home}/{action=Index}"); //定义路由
            return builder;
        }


        public string Name
        {
            get { return "Home"; }
        }

        public string Author { get { return "康泽"; } }
        public string AuthorEmail { get { return "kangze25@hotmail.com"; } }
        public string Url { get { return "http://www.baidu.com"; } }
    }
}

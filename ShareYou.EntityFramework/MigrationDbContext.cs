﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;
using ShareYou.EntityFramework.ShareYouDb;

namespace ShareYou.EntityFramework
{
    public class MigrationDbContext: IDbContextFactory<SyDbContext>
    {
        public SyDbContext Create()
        {
            return new SyDbContext("Server=DESKTOP-2I2UK5R;Database=ShareYou;Trusted_Connection=True;MultipleActiveResultSets=true");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AspNetCore.Identity.EntityFramework6;
using ShareYou.Entity.UserInfo;

namespace ShareYou.EntityFramework.ShareYouDb
{

    /*
     * **** 使用数据库迁移的时候要注意一点，fluent Api写在SyDbcontext中,不用调用base.on... DbContextBase继承自IdentityDbContext
     */
    public class SyDbContext : DbContextBase
    {
        static SyDbContext()
        {
            //设置策略
            System.Data.Entity.Database.SetInitializer(new CreateDatabaseIfNotExists<SyDbContext>());
        }

        public SyDbContext(string connectString) : base(connectString)
        {

        }
        /// <summary>
        /// 使用sqlserver数据库
        /// </summary>
        public override  string DataBaseType
        {
            get { return "SqlService"; }
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Entity<UserInfo>().HasKey(u => u.Id);
            builder.Entity<UserInfo>().Property(u => u.NormalizedUserName)
                .HasMaxLength(256)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("UserNameIndex") { IsUnique = true }));
            builder.Entity<UserInfo>().Property(u => u.ConcurrencyStamp).IsConcurrencyToken();
            builder.Entity<UserInfo>().Property(u => u.NormalizedEmail)
                .HasMaxLength(256)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("EmailIndex") { IsUnique = true }));
            builder.Entity<UserInfo>().ToTable("SY_UserInfo");
            builder.Entity<UserInfo>().Property(u => u.UserName).HasMaxLength(256);
            builder.Entity<UserInfo>().Property(u => u.Email).HasMaxLength(256);
            builder.Entity<UserInfo>().HasMany(u => u.Claims).WithRequired().HasForeignKey(u => u.UserId);
            builder.Entity<UserInfo>().HasMany(u => u.Logins).WithRequired().HasForeignKey(u => u.UserId);
            builder.Entity<UserInfo>().HasMany(u => u.Roles).WithRequired().HasForeignKey(u => u.UserId);


            builder.Entity<RoleInfo>().HasKey(u => u.Id);
            builder.Entity<RoleInfo>().Property(u => u.NormalizedName)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("RoleNameIndex") { IsUnique = true }));
            builder.Entity<RoleInfo>().ToTable("SY_RoleInfo");
            builder.Entity<RoleInfo>().Property(u => u.ConcurrencyStamp).IsConcurrencyToken();

            builder.Entity<RoleInfo>().Property(u => u.Name).HasMaxLength(256);
            builder.Entity<RoleInfo>().Property(u => u.NormalizedName).HasMaxLength(256);
            builder.Entity<RoleInfo>().HasMany(u => u.Users).WithRequired().HasForeignKey(u => u.RoleId);
            builder.Entity<RoleInfo>().HasMany(u => u.Claims).WithRequired().HasForeignKey(u => u.RoleId);

            builder.Entity<IdentityRoleClaim>().HasKey(u => u.Id);
            builder.Entity<IdentityRoleClaim>().ToTable("SY_RoleClaim");

            builder.Entity<IdentityUserRole>().HasKey(r => new { r.UserId, r.RoleId });
            builder.Entity<IdentityUserRole>().ToTable("SY_UserRole");

            builder.Entity<IdentityUserLogin>().HasKey(r => new { r.LoginProvider, r.ProviderKey });
            builder.Entity<IdentityUserLogin>().ToTable("SY_UserLogin");

            builder.Entity<IdentityUserToken>().HasKey(u => new { u.UserId, u.LoginProvider, u.Name });
            builder.Entity<IdentityUserToken>().ToTable("SY_UserToken");

            builder.Entity<IdentityUserClaim>().HasKey(u => u.Id);
            builder.Entity<IdentityUserClaim>().ToTable("SY_UserClaim");

        }
    }

}

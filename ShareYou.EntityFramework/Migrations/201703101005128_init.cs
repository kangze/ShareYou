namespace ShareYou.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SY_RoleClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SY_RoleInfo", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.SY_RoleInfo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        NormalizedName = c.String(maxLength: 256),
                        ConcurrencyStamp = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.NormalizedName, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.SY_UserRole",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.SY_RoleInfo", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.SY_UserInfo", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.SY_UserClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SY_UserInfo", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.SY_UserLogin",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        ProviderDisplayName = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey })
                .ForeignKey("dbo.SY_UserInfo", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.SY_UserInfo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RegisterDateTime = c.DateTime(nullable: false),
                        UserName = c.String(maxLength: 256),
                        NormalizedUserName = c.String(maxLength: 256),
                        Email = c.String(maxLength: 256),
                        NormalizedEmail = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        ConcurrencyStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEnd = c.DateTimeOffset(precision: 7),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.NormalizedUserName, unique: true, name: "UserNameIndex")
                .Index(t => t.NormalizedEmail, unique: true, name: "EmailIndex");
            
            CreateTable(
                "dbo.SY_UserToken",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 128),
                        Value = c.String(),
                    })
                .PrimaryKey(t => new { t.UserId, t.LoginProvider, t.Name });
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SY_UserRole", "UserId", "dbo.SY_UserInfo");
            DropForeignKey("dbo.SY_UserLogin", "UserId", "dbo.SY_UserInfo");
            DropForeignKey("dbo.SY_UserClaim", "UserId", "dbo.SY_UserInfo");
            DropForeignKey("dbo.SY_UserRole", "RoleId", "dbo.SY_RoleInfo");
            DropForeignKey("dbo.SY_RoleClaim", "RoleId", "dbo.SY_RoleInfo");
            DropIndex("dbo.SY_UserInfo", "EmailIndex");
            DropIndex("dbo.SY_UserInfo", "UserNameIndex");
            DropIndex("dbo.SY_UserLogin", new[] { "UserId" });
            DropIndex("dbo.SY_UserClaim", new[] { "UserId" });
            DropIndex("dbo.SY_UserRole", new[] { "RoleId" });
            DropIndex("dbo.SY_UserRole", new[] { "UserId" });
            DropIndex("dbo.SY_RoleInfo", "RoleNameIndex");
            DropIndex("dbo.SY_RoleClaim", new[] { "RoleId" });
            DropTable("dbo.SY_UserToken");
            DropTable("dbo.SY_UserInfo");
            DropTable("dbo.SY_UserLogin");
            DropTable("dbo.SY_UserClaim");
            DropTable("dbo.SY_UserRole");
            DropTable("dbo.SY_RoleInfo");
            DropTable("dbo.SY_RoleClaim");
        }
    }
}

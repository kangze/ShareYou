﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ShareYou.Entity.UserInfo;
using AspNetCore.Identity.EntityFramework6;

namespace ShareYou.EntityFramework
{
    /// <summary>
    /// 主框架的数据库上下文,使用EntityFramework,上一级封装了这个Identity相关的内容
    /// </summary>
    public abstract class DbContextBase:IdentityDbContext<UserInfo, RoleInfo,int,IdentityUserClaim,IdentityUserRole, IdentityUserLogin, IdentityRoleClaim, IdentityUserToken>
    {
        
        protected DbContextBase(string connectiongString):base(connectiongString)
        {
            
        }
        /// <summary>
        /// 使用的数据库类型,sqlsqlver,mysql,
        /// </summary>
        public abstract string DataBaseType { get; }

        

        protected override void OnModelCreating(DbModelBuilder builder)
        {

            
            base.OnModelCreating(builder);

        }
    }
}

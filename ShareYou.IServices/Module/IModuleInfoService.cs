﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShareYou.Entity.Module;

namespace ShareYou.IServices.Module
{
    /// <summary>
    /// 模块service
    /// </summary>
    public interface IModuleInfoService
    {
        /// <summary>
        /// 添加保存模块信息到主程序中
        /// </summary>
        /// <param name="info"></param>
        void Add(ModuleInfo info);
        /// <summary>
        /// 添加模块信息
        /// </summary>
        /// <param name="infos"></param>
        void Add(IList<ModuleInfo> infos);
        /// <summary>
        /// 获取模块信息
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        ModuleInfo Get(string name);

        /// <summary>
        /// 获取模块信息
        /// </summary>
        /// <param name="author"></param>
        /// <returns></returns>
        IList<ModuleInfo> Gets(string author);
    }
}
